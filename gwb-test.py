
import logging
import pendulum
from airflow.decorators import dag, task

DEFAULT_ARGS = {
    "owner": "gbilder",
    "depends_on_past": False,
    "email": ["gbilder@crossref.org"],
    "email_on_failure": False,
}

@dag(dag_id="gwb-test", default_args=DEFAULT_ARGS, schedule="@daily", start_date=pendulum.datetime(2023, 2, 9, tz="UTC"), catchup=True, tags=["labs", "experiments"], owner_links={"gbilder": "https://www.crossref.org/people/geoffrey-bilder/"}, description="test dag in AWS")
def gwb_test_dag():

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)


    @task.virtualenv(
           
            system_site_packages=True,
            requirements=["boto3","pandas"],
        )
    def test_task_one():
        import sys
        import logging
        # from crutils.gwb import MAILTO
        #import boto3
        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger(__name__)
        logger.info(f"**** path: {sys.path}")
        return "test task one from its own repo"
    
    # @task()
    # def test_task_two():
    #     #import logging
    #     #logger = logging.getLogger(__name__)
    #     #logger.info("**** test task two")

    test_task_one()

gwb_test_dag()
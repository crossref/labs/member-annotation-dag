import base64
import json
import boto3

class S3ObjException(Exception):
    pass

def get_secret(secret_name,profile_name):
    """
    return AWS secrets in usable form
    """
    region_name = "us-east-1"

    session = boto3.session.Session(profile_name=profile_name)
    client = session.client(service_name="secretsmanager", region_name=region_name)

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if "SecretString" in get_secret_value_response:
        return get_secret_value_response["SecretString"]
    else:
        return base64.b64decode(get_secret_value_response["SecretBinary"])


# def store(bucket, fn, results, profile_name):
#     session = boto3.session.Session(profile_name=profile_name)
#     s3 = session.resource("s3")
#     obj = s3.Object(bucket, fn)
#     obj.put(Body=json.dumps(results))

def store(bucket, fn, results, session):
    # session = boto3.session.Session(profile_name=profile_name)
    s3 = session.resource("s3")
    obj = s3.Object(bucket, fn)
    obj.put(Body=json.dumps(results))

def load(bucket, fn, session):
    try:
        # session = boto3.session.Session(profile_name=profile_name)
        s3 = session.resource("s3")
        obj = s3.Object(bucket, fn)
        return obj.get()["Body"].read().decode("utf-8")
    except Exception as e:
        raise S3ObjException(e) from e
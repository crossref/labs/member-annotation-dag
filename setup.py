import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='crutils',
    version='0.0.1',
    author='Geoffrey Bilder',
    author_email='gbilder@crossref.org',
    description='common utilities for airflow',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/crossref/labs/member-annotation-dag/',
    project_urls = {
        "Home": "https://gitlab.com/crossref/labs/member-annotation-dag/-/tree/main/"
    },
    license='MIT',
    packages=['crutils'],
    install_requires=['requests','boto3'],
)